//VIEW(GUI)

package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
	private JLabel showProjectName;
	private JTextArea showResults;
	private JButton endButton;
	private String str;
   
   public InvestmentFrame()
   { 
      createFrame();
      
      setVisible(true);

   }
 
	public void createFrame() {
		this.showProjectName = new JLabel("Lab01");
		this.showResults = new JTextArea("your resutls will be showed here");
		setLayout(new BorderLayout());
		add(this.showProjectName, BorderLayout.NORTH);
		add(this.showResults, BorderLayout.CENTER);
	}  
   
	public void setResult(String str) {
		this.str = str;
		showResults.setText(this.str);
	}
	
	public void extendResult(String str) {
		this.str = this.str + "\n" + str;
		showResults.setText(this.str);
	}
}
