//CONTROLLER+main

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import view.InvestmentFrame;
import model.HashingModel;

public class Controller {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Controller();
	}

	public Controller() {
		frame = new InvestmentFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(600,1000);
		setTestCase();
	}

	public void setTestCase() {
		HashingModel hash = new HashingModel();
		int test1 = hash.ascii("http://www.cs.sci.ku.ac.th/~fscichj/"); 
		int test2 = hash.ascii("https://www.facebook.com/"); 
		int test3 = hash.ascii("http://steamcommunity.com/sharedfiles/filedetails/?id=282403193"); 
		int test4 = hash.ascii("http://www.psychiclibrary.com/beyondBooks/flash/index.html"); 
		int test5 = hash.ascii("https://www.fanfiction.net/s/10545421/1/A-Box-of-Figs"); 
		int test6 = hash.ascii("https://www.youtube.com/channel/UCY30JRSgfhYXA6i6xX1erWg"); 
		int test7 = hash.ascii("http://www.lalala.com"); 
		int test8 = hash.ascii("http://www.ku.ac.th"); 
		int test9 = hash.ascii("http://www.google.com"); 
		int test10 = hash.ascii("http://www.regis.ku.ac.th"); 
		int test11 = hash.ascii("http://www.sereneforest.com"); 
		int test12 = hash.ascii("http://www.wikipedia.org"); 
		
		frame.setResult("http://www.cs.sci.ku.ac.th/~fscichj/"+"\n"+"Total ASCII : "+test1+"\n"+"Hashing code : "+hash.hashing(test1)+"\n");
		frame.extendResult("https://www.facebook.com/"+"\n"+"Total ASCII : "+test2+"\n"+"Hashing code : "+hash.hashing(test2));
		frame.extendResult("http://steamcommunity.com/sharedfiles/filedetails/?id=282403193"+"\n"+"Total ASCII : "+test3+"\n"+"Hashing code : "+hash.hashing(test3)+"\n");
		frame.extendResult("http://www.psychiclibrary.com/beyondBooks/flash/index.html"+"\n"+"Total ASCII : "+test4+"\n"+"Hashing code : "+hash.hashing(test4)+"\n");
		frame.extendResult("https://www.fanfiction.net/s/10545421/1/A-Box-of-Figs"+"\n"+"Total ASCII : "+test5+"\n"+"Hashing code : "+hash.hashing(test5)+"\n");
		frame.extendResult("https://www.youtube.com/channel/UCY30JRSgfhYXA6i6xX1erWg"+"\n"+"Total ASCII : "+test6+"\n"+"Hashing code : "+hash.hashing(test6)+"\n");
		frame.extendResult("http://www.lalala.com"+"\n"+"Total ASCII : "+test7+"\n"+"Hashing code : "+hash.hashing(test7)+"\n");
		frame.extendResult("http://www.ku.ac.th"+"\n"+"Total ASCII : "+test8+"\n"+"Hashing code : "+hash.hashing(test8)+"\n");
		frame.extendResult("http://www.google.com"+"\n"+"Total ASCII : "+test9+"\n"+"Hashing code : "+hash.hashing(test9)+"\n");
		frame.extendResult("http://www.regis.ku.ac.th"+"\n"+"Total ASCII : "+test10+"\n"+"Hashing code : "+hash.hashing(test10)+"\n");
		frame.extendResult("http://www.sereneforest.com"+"\n"+"Total ASCII : "+test11+"\n"+"Hashing code : "+hash.hashing(test10)+"\n");
		frame.extendResult("http://www.wikipedia.org"+"\n"+"Total ASCII : "+test12+"\n"+"Hashing code : "+hash.hashing(test12)+"\n");
		//frame.setResult("http://www.cs.sci.ku.ac.th/~fscichj/"+hash.ascii("http://www.cs.sci.ku.ac.th/~fscichj/")+hash.hashing(hash.ascii("http://www.cs.sci.ku.ac.th/~fscichj/")));
	}
	
	ActionListener list;
	InvestmentFrame frame;
}